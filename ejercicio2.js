function clasificarRuedas (diametro) {
    if (diametro <= 10) {
        console.log('Es una rueda para un juguete pequeño.')
    } 
    if (diametro > 10 && diametro < 20) {
        console.log('Es una rueda para un juguete mediano.')
    }
    if (diametro >= 20) {
        console.log('Es una rueda para un juguete grande.')
    }
}

//Diferentes pruebas
clasificarRuedas(-10);
clasificarRuedas(1);
clasificarRuedas(10);
clasificarRuedas(11);
clasificarRuedas(19);
clasificarRuedas(20);
clasificarRuedas(25);
clasificarRuedas(50);
